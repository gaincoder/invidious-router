
FROM golang:1.22-alpine

WORKDIR /app

COPY go.mod .
COPY go.sum .

RUN go mod download

COPY . .

RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build
RUN apk --update add ca-certificates



FROM scratch
WORKDIR /app
COPY --from=0 /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/ca-certificates.crt
COPY --from=0 /app/invidious-router /app/invidious-router
ENV HTTP_PORT=8050
EXPOSE 8050

ENTRYPOINT ["./invidious-router"]
