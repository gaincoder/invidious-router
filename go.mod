module gitlab.com/gaincoder/invidious-router

go 1.22

require gopkg.in/yaml.v2 v2.4.0

require (
	github.com/inconshreveable/mousetrap v1.1.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
)
