package instanceapi

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"

	"gitlab.com/gaincoder/invidious-router/internal/config"
)

type instance struct {
	Region string `json:"region"`
	Type   string `json:"type"`
	Uri    string `json:"uri"`
}

type Entry struct {
	Domain   string `json:"-"`
	Instance instance
}

var Cfg config.Api

func isValidInstance(e Entry) bool {
	return e.Instance.Type == "https" && filterRegion(e)
}

func filterRegion(e Entry) bool {
	if Cfg.FilterRegion == false {
		return true
	}
	for _, r := range Cfg.Regions {
		if e.Instance.Region == r {
			return true
		}
	}
	return false
}

func createRequest() (*http.Request, error) {
	req, err := http.NewRequest("GET", Cfg.Url, nil)
	if err != nil {
		return nil, err
	}

	return req, nil
}

func getApiResponse(req *http.Request) (*http.Response, error) {
	client := &http.Client{}
	response, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	return response, nil
}

func parseResponse(response *http.Response) ([][]json.RawMessage, error) {
	var instances [][]json.RawMessage
	err := json.NewDecoder(response.Body).Decode(&instances)
	if err != nil {
		return nil, err
	}

	return instances, nil
}

func unmarshalEntries(instances [][]json.RawMessage) ([]Entry, error) {
	var entries []Entry
	for _, d := range instances {
		var entry Entry
		err := json.Unmarshal(d[0], &entry.Domain)
		if err != nil {
			return nil, err
		}
		err = json.Unmarshal(d[1], &entry.Instance)
		if err != nil {
			return nil, err
		}
		entries = append(entries, entry)
	}

	return entries, nil
}

func filterValidInstances(entries []Entry) ([]*url.URL, error) {
	var hosts []*url.URL
	for _, e := range entries {
		if isValidInstance(e) {
			url, err := url.Parse(e.Instance.Uri)
			if err != nil {
				return nil, err
			}
			hosts = append(hosts, url)
		}
	}

	return hosts, nil
}

func GetPossibleHostsFromApi() []*url.URL {
	var hosts []*url.URL
	if Cfg.Enabled {
		req, err := createRequest()
		if err != nil {
			fmt.Println(err)
			return nil
		}

		response, err := getApiResponse(req)
		if err != nil {
			panic(err)
		}

		instances, err := parseResponse(response)
		if err != nil {
			fmt.Println(err)
			return nil
		}

		entries, err := unmarshalEntries(instances)
		if err != nil {
			fmt.Println(err)
			return nil
		}

		hosts, err = filterValidInstances(entries)
		if err != nil {
			fmt.Println(err)
			return nil
		}

	}
	return hosts
}
