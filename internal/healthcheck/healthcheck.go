package healthcheck

import (
	"encoding/json"
	"io"
	"net/http"
	"net/url"
	"sort"
	"strings"
	"time"

	"gitlab.com/gaincoder/invidious-router/internal/config"
)

type TimeRemote struct {
	Remote  *url.URL
	Elapsed float64
}

type Invidious struct {
	Version  string `json:"version"`
	Playback struct {
		Ratio *float64 `json:"ratio"`
	} `json:"playback"`
}

var Cfg config.Healthcheck

func ScoreRemotes(remotes []*url.URL) []*url.URL {
	scoredRemotes := callRemotes(remotes)
	if Cfg.FilterByResponseTime.Enabled {
		scoredRemotes = sortTimeRemotesByElapsed(scoredRemotes)
		scoredRemotes = filterBestXTimeRemotes(scoredRemotes, Cfg.FilterByResponseTime.Qty)
	}
	remotes = convertTimeRemotesToRemotes(scoredRemotes)
	return remotes
}

func sortTimeRemotesByElapsed(timeRemotes []TimeRemote) []TimeRemote {
	sort.Slice(timeRemotes, func(i, j int) bool {
		return timeRemotes[i].Elapsed < timeRemotes[j].Elapsed
	})
	return timeRemotes
}

func filterBestXTimeRemotes(timeRemotes []TimeRemote, x int) []TimeRemote {
	if x > len(timeRemotes) {
		x = len(timeRemotes)
	}
	return timeRemotes[:x]
}

func convertTimeRemotesToRemotes(timeRemotes []TimeRemote) []*url.URL {
	var remotes []*url.URL
	for _, remote := range timeRemotes {
		remotes = append(remotes, remote.Remote)
	}
	return remotes
}

func callRemotes(remotes []*url.URL) []TimeRemote {
	var scoredRemotes []TimeRemote
	for _, remote := range remotes {
		start := time.Now()
		client := http.Client{
			Timeout: Cfg.TimeOut,
		}
		response, err := client.Get(remote.String() + Cfg.Path)
		elapsed := time.Since(start).Seconds()
		if err == nil {
			if checkAll(response, remote) {
				timeRemote := TimeRemote{Remote: remote, Elapsed: elapsed}
				scoredRemotes = append(scoredRemotes, timeRemote)
			}
		}
	}
	return scoredRemotes
}

func checkStatusCode(code int) bool {
	for _, allowedCode := range Cfg.AllowedStatusCodes {
		if code == allowedCode {
			return true
		}
	}
	return false
}

func checkTextNotPresent(text io.ReadCloser) bool {
	if Cfg.TextNotPresent == "" {
		return true
	}
	body, err := io.ReadAll(text)
	if err != nil {
		return false
	}
	return !strings.Contains(string(body), Cfg.TextNotPresent)
}

func checkRatio(remote *url.URL) bool {
	client := http.Client{}
	response, err := client.Get(remote.String() + "/api/v1/stats")
	if err != nil {
		return false
	}
	data := parseResponse(response)

	var ratio float64
	ratio = -1
	if !Cfg.RemoveNoRatio {
		ratio = Cfg.MinimumRatio
	}
	if data.Playback.Ratio != nil {
		ratio = *data.Playback.Ratio
	}
	return ratio >= Cfg.MinimumRatio
}

func parseResponse(response *http.Response) Invidious {
	var data Invidious
	json.NewDecoder(response.Body).Decode(&data)
	return data
}

func checkAll(response *http.Response, remote *url.URL) bool {
	statusOk := checkStatusCode(response.StatusCode)
	textOk := checkTextNotPresent(response.Body)
	ratioOk := checkRatio(remote)
	return statusOk && textOk && ratioOk
}
