package instancelist

import (
	"fmt"
	"net/url"
	"time"

	"gitlab.com/gaincoder/invidious-router/internal/config"
)

type InstanceCollectorFn func() []*url.URL

type InstanceFilterFn func([]*url.URL) []*url.URL

type HostList struct {
	hosts           []*url.URL
	allHosts        []*url.URL
	lastListRefresh time.Time
	lastHealthcheck time.Time
}

var Cfg config.Configuration

func NewHostList(collectors []InstanceCollectorFn, filters []InstanceFilterFn) *HostList {
	hostlist := HostList{}
	hostlist.setAllHosts(callCollectors(collectors))
	hostlist.setHosts(callFilters(filters, hostlist.GetAllHosts()))
	hostlist.lastListRefresh = time.Now()
	hostlist.lastHealthcheck = time.Now()
	go func() {
		for {
			time.Sleep(Cfg.App.ReloadList)
			hostlist.setAllHosts(callCollectors(collectors))
			hostlist.setHosts(callFilters(filters, hostlist.GetAllHosts()))
			hostlist.lastListRefresh = time.Now()
			hostlist.lastHealthcheck = time.Now()
			fmt.Println(hostlist.GetHosts())
		}
	}()
	go func() {
		for {
			time.Sleep(Cfg.Healthcheck.Interval)
			hostlist.setHosts(callFilters(filters, hostlist.GetAllHosts()))
			hostlist.lastHealthcheck = time.Now()
		}
	}()
	return &hostlist
}

func (hostlist *HostList) setHosts(hosts []*url.URL) {
	hostlist.hosts = hosts
}

func (hostlist *HostList) setAllHosts(hosts []*url.URL) {
	hostlist.allHosts = hosts
}

func (hostlist *HostList) GetHosts() []*url.URL {
	return hostlist.hosts
}

func (hostlist *HostList) GetAllHosts() []*url.URL {
	return hostlist.allHosts
}

func (hostlist *HostList) AddHost(host *url.URL) {
	hostlist.hosts = append(hostlist.hosts, host)
}

func (hostlist *HostList) addHosts(hosts []*url.URL) {
	hostlist.hosts = append(hostlist.hosts, hosts...)
}

func (hostlist *HostList) LastListRefresh() string {
	return hostlist.lastListRefresh.Format("2006-01-02 15:04:05")
}

func (hostlist *HostList) LastHealthcheck() string {
	return hostlist.lastHealthcheck.Format("2006-01-02 15:04:05")
}

func callCollectors(collectors []InstanceCollectorFn) []*url.URL {
	var hosts []*url.URL
	for _, collector := range collectors {
		hosts = append(hosts, collector()...)
	}
	return hosts
}

func callFilters(filters []InstanceFilterFn, hosts []*url.URL) []*url.URL {
	for _, filter := range filters {
		hosts = filter(hosts)
	}
	return hosts
}
