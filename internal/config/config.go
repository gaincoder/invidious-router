package config

import (
	"embed"
	"fmt"
	"net/url"
	"os"
	"path/filepath"
	"time"

	"gopkg.in/yaml.v2"
)

//go:embed build
var fs embed.FS

var ConfigPath string

type (
	App struct {
		YouTubeFallback     bool          `yaml:"enable_youtube_fallback"`
		Listen              string        `yaml:"listen"`
		ReloadList          time.Duration `yaml:"reload_instance_list_interval"`
		Instances           []string      `yaml:"instances"`
		Blacklist           []string      `yaml:"blacklist"`
		NotAvailableMessage string        `yaml:"not_available_message"`
	}

	Api struct {
		Enabled      bool     `yaml:"enabled"`
		Url          string   `yaml:"url"`
		FilterRegion bool     `yaml:"filter_regions"`
		Regions      []string `yaml:"allowed_regions"`
	}

	filterByResponseTime struct {
		Enabled bool `yaml:"enabled"`
		Qty     int  `yaml:"qty_of_top_results"`
	}

	Healthcheck struct {
		Path                 string               `yaml:"path"`
		AllowedStatusCodes   []int                `yaml:"allowed_status_codes"`
		TimeOut              time.Duration        `yaml:"timeout"`
		Interval             time.Duration        `yaml:"interval"`
		FilterByResponseTime filterByResponseTime `yaml:"filter_by_response_time"`
		TextNotPresent       string               `yaml:"text_not_present"`
		MinimumRatio         float64              `yaml:"minimum_ratio"`
		RemoveNoRatio        bool                 `yaml:"remove_no_ratio"`
	}

	Configuration struct {
		App         App         `yaml:"app"`
		Api         Api         `yaml:"api"`
		Healthcheck Healthcheck `yaml:"healthcheck"`
	}
)

var Cfg Configuration

func LoadConfig() (Configuration, error) {
	config := Configuration{}

	_, err := os.Stat(ConfigPath)
	if os.IsNotExist(err) {
		file, err := fs.ReadFile("build/config.yaml.default")
		if err != nil {
			fmt.Println(err)
			panic("Cannot read default config file. Exiting.")
		}
		dir := filepath.Dir(ConfigPath)
		_, err = os.Stat(dir)
		if os.IsNotExist(err) {
			err = os.Mkdir(dir, 0755)
			if err != nil {
				fmt.Println(err)
				panic("Cannot create config directory. Exiting.")
			}
		}
		err = os.WriteFile(ConfigPath, file, 0644)
		if err != nil {
			fmt.Println(err)
			panic("Cannot create config file. Exiting.")
		}
		fmt.Println("Default config file created at " + ConfigPath)
	}
	file, err := os.ReadFile(ConfigPath)
	if err != nil {
		return config, err
	}

	err = yaml.Unmarshal(file, &config)
	if err != nil {
		return config, err
	}
	Cfg = config
	return config, nil
}

func GetCurrentInstances() []*url.URL {
	urls := []*url.URL{}
	_, err := LoadConfig()
	if err != nil {
		return urls
	}
	for _, inst := range Cfg.App.Instances {
		url, err := url.Parse(inst)
		if err == nil {
			urls = append(urls, url)
		}
	}
	return urls
}

func FilterForBlacklist(input []*url.URL) []*url.URL {
	blacklist := Cfg.App.Blacklist
	blacklistMap := make(map[string]bool)
	for _, item := range blacklist {
		blacklistMap[item] = true
	}

	result := make([]*url.URL, 0)
	for _, item := range input {
		key := item.String()
		if _, found := blacklistMap[key]; !found {
			result = append(result, item)
		}
	}

	return result
}
