// main_test.go
package main

import (
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"

	"gitlab.com/gaincoder/invidious-router/internal/instancelist"
)

func TestCreateHandler(t *testing.T) {
	// Create a mock HostList
	hostList := &instancelist.HostList{}
	testUrl, _ := url.Parse("https://example.com")
	hostList.AddHost(testUrl)
	// Create the handler
	handler := createHandler(hostList)

	// Create a mock request
	req, err := http.NewRequest("GET", "/", nil)
	if err != nil {
		t.Fatalf("Could not create request: %v", err)
	}

	// Record the response
	rec := httptest.NewRecorder()
	handler.ServeHTTP(rec, req)

	// Check the response status code
	if status := rec.Code; status != http.StatusFound {
		t.Errorf("Handler returned wrong status code: got %v want %v", status, http.StatusFound)
	}
}
