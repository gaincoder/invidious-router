package main

import (
	"fmt"
	"math/rand"
	"net/http"
	"net/url"
	"strings"

	flag "github.com/spf13/pflag"

	"gitlab.com/gaincoder/invidious-router/internal/config"
	"gitlab.com/gaincoder/invidious-router/internal/healthcheck"
	"gitlab.com/gaincoder/invidious-router/internal/instanceapi"
	"gitlab.com/gaincoder/invidious-router/internal/instancelist"
	"gopkg.in/yaml.v2"
)

var Cfg config.Configuration

func main() {

	var err error
	cfgpath := flag.StringP("configfile", "c", "./config/config.yaml", "Path to the configuration file")
	flag.Parse()
	config.ConfigPath = *cfgpath
	fmt.Println("Configuration path is:", config.ConfigPath)

	Cfg, err = config.LoadConfig()
	if err != nil {
		panic(err)
	}
	instancelist.Cfg = Cfg
	instanceapi.Cfg = Cfg.Api
	collectors := []instancelist.InstanceCollectorFn{config.GetCurrentInstances, instanceapi.GetPossibleHostsFromApi}

	healthcheck.Cfg = Cfg.Healthcheck
	filters := []instancelist.InstanceFilterFn{config.FilterForBlacklist, healthcheck.ScoreRemotes}

	hostList := instancelist.NewHostList(collectors, filters)

	handler := createHandler(hostList)
	fmt.Println("Listening on http://" + Cfg.App.Listen)
	err = http.ListenAndServe(Cfg.App.Listen, handler)
	if err != nil {
		panic(err)
	}

}

func createHandler(hostList *instancelist.HostList) http.Handler {
	mux := http.NewServeMux()
	mux.HandleFunc("/", webHandler(hostList))
	mux.HandleFunc("/router-status", statusHandler(hostList))
	mux.HandleFunc("/router-config", configHandler())
	return mux
}

func prettyPrint(i interface{}) string {
	s, _ := yaml.Marshal(i)
	return string(s)
}

func webHandler(hostlist *instancelist.HostList) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		urls := hostlist.GetHosts()
		targetHost := selectRandomHost(urls)
		if targetHost == "--no-instances-available--" {
			w.WriteHeader(200)
			w.Header().Add("Content-Type", "text/html")
			text := Cfg.App.NotAvailableMessage
			if text == "" {
				text = "No available invidious instance found!"
			}
			text = strings.Replace(text, "[link]", "<a href=\""+buildTargetURL("https://youtube.com", r).String()+"\">", 1)
			text = strings.Replace(text, "[/link]", "</a>", 1)
			text = html(text)
			_, err := w.Write([]byte(text))
			if err != nil {
				fmt.Println(err)
			}
			return
		}
		targetURL := buildTargetURL(targetHost, r)
		w.Header().Set("Location", targetURL.String())
		w.WriteHeader(302)
	}
}

func statusHandler(hostlist *instancelist.HostList) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		urls := hostlist.GetHosts()
		html := "<html><head><title>Router Status</title></head><body><h1>Router Status</h1>"
		html += "<p>Checked instances (refreshed " + hostlist.LastListRefresh() + "):</p><ul>"
		for _, u := range hostlist.GetAllHosts() {
			html += "<li>" + u.String() + "</li>"
		}
		html += "</ul>"
		if len(urls) == 0 {
			html += "<p>No instances available</p>"
		} else {
			html += "<p>Selected instances (refreshed " + hostlist.LastHealthcheck() + "):</p><ul>"
			for _, u := range urls {
				html += "<li>" + u.String() + "</li>"
			}
			html += "</ul>"
		}
		html += "</body></html>"
		_, err := w.Write([]byte(html))
		if err != nil {
			fmt.Println(err)
		}
	}
}

func configHandler() func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		html := "<html><head><title>Router Status</title></head><body><h1>Router Config</h1><pre>"
		html += prettyPrint(Cfg)
		html += "</pre></body></html>"
		_, err := w.Write([]byte(html))
		if err != nil {
			fmt.Println(err)
		}
	}
}

func buildTargetURL(target string, r *http.Request) *url.URL {
	if r.URL.Path != "" {
		target += r.URL.Path
	}
	if r.URL.RawQuery != "" {
		target += "?" + r.URL.RawQuery
	}
	url, err := url.Parse(target)
	if err != nil {
		panic("Invalid target URL")
	}
	return url
}

func selectRandomHost(urls []*url.URL) string {
	fmt.Println(Cfg.App.YouTubeFallback)
	if len(urls) == 0 {
		if Cfg.App.YouTubeFallback {
			return "https://www.youtube.com"
		} else {
			return "--no-instances-available--"
		}
	}
	targetHost := urls[rand.Intn(len(urls))]
	return targetHost.String()
}

func html(text string) string {
	return `<!DOCTYPE html> 
			<html>
			<head>
				<meta charset=\"utf-8\">
				<title>Invidious Router</title>
			</head>
			<body>
				` + text + `
			</body>
			</html>`
}
