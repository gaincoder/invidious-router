# Invidious Router

Invidious Router is a Go application that routes requests to different [Invidious](https://invidious.io/) instances based on their health status and (optional) response time.

## Packages
[![Packaging status](https://repology.org/badge/vertical-allrepos/invidious-router.svg)](https://repology.org/project/invidious-router/versions)

## Demo

You can find a running instance of this software at [https://video.fosswelt.org/](https://video.fosswelt.org/) .

For example you can watch a video about Ada & Zangemann: [https://video.fosswelt.org/watch?v=5ouMaWXKelU](https://video.fosswelt.org/watch?v=5ouMaWXKelU)

## Features

- **Health Check**: The application periodically checks the health of each Invidious instance by 
   - sending a GET request to a specified path and checking the response status code
   - check response for a text if the instance is currently blocked by YT
   - check the ratio value for successful requests
- **Response Time Filtering**: The application can filter out the Invidious instances that have the slowest response times.
- **Instanceliste from API**: By default the [API from Invidious](https://api.invidious.io/) is used as instance-list
    - **Region Filtering**: The application can filter out Invidious instances of the api based on their region.
- **Custom Instancelist**: You can set a list of instances which should be uses (standalone or togehter with the list from the API)
- **Load Balancing**: The application evenly distributes requests among the available Invidious instances to prevent overloading a single instance.
- **Automatic Recovery**: If an Invidious instance goes down but later becomes available again, the application will automatically start routing requests to it.
- **Configurable**: The behavior of the application can be customized through a configuration file.
- **YouTube Fallback**: It's possible to fallback to YouToube if no instance is available (disabled by default)
- **Instance Blacklist**: A list of instances which should be ignored

## Getting Started

### Option 1: Run locally

1. Download the latest version for your operating system on the [releases page](https://gitlab.com/gaincoder/invidious-router/-/releases) or install your distro-specific package.
2. (on linux) Add permissions to execute
   ```sh
   sudo chmod +x invidious-router
   ```
3. Run it via command line
   ```sh
   ./invidious-router
   ```
4. Open [http://localhost:8050/](http://localhost:8050/) in your browser

### Option 2: Run on docker

1. Run it via command line
   ```sh
   docker run gaincoder/invidious-router
   ```
2. Open [http://localhost:8050/](http://localhost:8050/) in your browser

### Option 3: Installation from source-code

1. Clone the repository:
   ```sh
   git clone https://gitlab.com/gaincoder/invidious-router.git
   ```
2. Change into the project directory:
   ```sh
   cd invidious-router
   ```
3. Build the application:
   ```sh
   go build -o invidious-router
   ```
4. follow steps 2 - 4 of option 1

### Configuration

The application can be configured by modifying the config yaml file. It will be created on the first run. 

Default location: ./config/config.yaml

You can use the cli flag `-c` or `--configfile` to set the path of the configfile.

Here are the available configuration options:

| Configuration Key | Description | Example |
| --- | --- | --- |
| `app.listen` | The IP address and port the application should listen on. | `"0.0.0.0:8050"` |
| `app.enable_youtube_fallback` | Whether to fallback to YouTube if no instance is available. | `false` |
| `app.reload_instance_list_interval` | The interval at which the application should reload the instance list. | `"60s"` |
| `app.instances` | A list of Invidious instances to use. | `["https://invidious.private.coffee"]` |
| `app.blacklist` | A list of Invidious instances to exclude. | `["https://deadinstance.com"]` |
| `app.not_available_message` | Message Text if no instance is available. Optional with YT-Link | `No instance found. Open it on [link]YouTube[/link]` |
| `api.enabled` | Whether to enable the Invidious API. | `true` |
| `api.url` | The URL of the Invidious API. | `"https://api.invidious.io/instances.json"` |
| `api.filter_regions` | Whether to filter Invidious instances based on their region. | `true` |
| `api.allowed_regions` | A list of allowed regions. | `["AT", "DE", "CH"]` |
| `healthcheck.path` | The path to send the health check request to. | `"/"` |
| `healthcheck.allowed_status_codes` | A list of status codes that are considered healthy. | `[200]` |
| `healthcheck.timeout` | The timeout for the health check request. | `"1s"` |
| `healthcheck.interval` | The interval between health checks. | `"10s"` |
| `healthcheck.text_not_present` | The text which should not be on the page | `YouTube is currently trying to block Invidious instances` |
| `healthcheck.minimum_ratio` | Minimum value for ratio | `0.2` |
| `healthcheck.remove_no_ratio` | Don't use instances which don't provide a ratio | `true` |
| `healthcheck.filter_by_response_time.enabled` | Whether to filter Invidious instances based on their response time. | `true` |
| `healthcheck.filter_by_response_time.qty_of_top_results` | The number of Invidious instances with the fastest response times to keep. | `3` |

## Usage


The application will start an HTTP server on port 8050. 

You can open the root path (http://localhost:8500/) to be routed to a healthy Invidious instance or directly to a target path (e.g. http://localhost:8500/watch.. or http://localhost:8500/embed/...).


## Transparency

For transparency for end-users there are two endpoints available:
- **/router-status**: an overview of checked and currently selected instances
- **/router-config**: an overview of the configuration, e.g. to check if youtube-fallback is enables

## Contributing

Contributions are welcome! Please feel free to submit changes.

## License

This is open source software licensed under the [MIT license](https://tldrlegal.com/license/mit-license).
